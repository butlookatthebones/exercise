package com.db.model;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Created by lynnscott on 7/28/17.
 */
public class DataStore {

    private Map<String, Object> db = new HashMap<>();
    private Map<String, Map<String, Object>> transactions = new HashMap<>();

    public void put(String key, Object value) {
       db.put(key, value);
    }

    public void put(String key, Object value, String transactionId) {

        if (transactions.get(transactionId) == null) {
            createTransaction(transactionId);
        }

        Map<String, Object> transactionEntry = transactions.get(transactionId);
        if (transactionEntry.get(key) == null) {
            transactionEntry.put(key,value);
        }

        Map<String, Object> data = new HashMap<>();
        data.put(key,value);
        transactions.put(transactionId, data);
        return;
    }

    public Optional<Object> get(String key) {
        return Optional.ofNullable(db.get(key));

    }

    public Optional<Object> get(String key, String transactionId) throws NoSuchElementException {

        Map<String, Object> transactionEntry = transactions.get(transactionId);

        if (transactionEntry == null) {
            throw new NoSuchElementException();
        }
        return Optional.ofNullable(transactionEntry.get(key));
    }

    public void delete(String key) {
        db.remove(key);
        return;
    }

    public void delete(String key, String transactionId) throws NoSuchElementException {
        if (transactions.get(transactionId) != null) {
            Map<String, Object> transactionEntry = transactions.get(transactionId);
            transactionEntry.remove(key);
        } else {
            throw new NoSuchElementException();
        }
        return;
    }

    public void createTransaction(String transactionId) {

        Map<String, Object> transactionEntries = new HashMap<>();
        transactions.put(transactionId,transactionEntries);
        return;
    }

    public void rollBackTransaction(String transactionId) throws NoSuchElementException {
        Map<String, Object> entries = transactions.get(transactionId);

        if (entries == null) {
            throw new NoSuchElementException();
        }
        entries.clear();
        transactions.remove(transactionId);
        return;
    }

    public void commitTransaction(String transactionId) throws NoSuchElementException, IllegalStateException {

        Map<String, Object> entries = transactions.get(transactionId);
        if (entries == null) {
            throw new NoSuchElementException();
        }

        entries.forEach(
                (key, value) -> {
                    if (db.containsKey(key)) {
                        throw new IllegalStateException();
                    } else {
                        db.put(key,value);
                    }
                }
        );

        return;
    }

}
