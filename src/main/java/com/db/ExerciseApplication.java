package com.db;

import com.db.model.DataStore;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.NoSuchElementException;
import java.util.Optional;

@SpringBootApplication
public class ExerciseApplication implements CommandLineRunner {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(ExerciseApplication.class, args);
	}

	@Override
	public void run(String... args) {

		System.out.println("Example sequence without transactions:");
		DataStore myDb = new DataStore();
		myDb.put("example", "foo");
		String s = (String)myDb.get("example").get(); // returns "foo"
		System.out.println("Expected: \"foo\" Result: " + s);
		myDb.delete("example");
		Optional<Object> optional = myDb.get("example"); // returns empty Optional
		System.out.println("Expected: \"empty Optional\" Result: " + optional);
		myDb.delete("example");

		System.out.print("\n");
		System.out.println("Example sequence with transactions:");
		myDb = new DataStore();
		myDb.createTransaction("abc");
		myDb.put("a", "foo", "abc");
		s = (String)myDb.get("a", "abc").get(); // returns "foo"
		System.out.println("Expected: \"foo\" Result: " + s);
		optional = myDb.get("a"); // return empty Optional
		System.out.println("Expected: \"empty Optional\" Result: " + optional);

		myDb.createTransaction("xyz");
		myDb.put("a", "bar", "xyz");
		s = (String)myDb.get("a", "xyz").get(); // returns "bar"
		System.out.println("Expected: \"bar\" Result: " + s);

		myDb.commitTransaction("xyz");
		s = (String)myDb.get("a").get(); // returns "bar"
		System.out.println("Expected: \"bar\" Result: " + s);

		try {
			myDb.commitTransaction("abc"); // throws exception
		} catch (NoSuchElementException | IllegalStateException ex) {
			//swollow exception
		}
		s = (String)myDb.get("a").get(); // returns "bar"
		System.out.println("Expected: \"bar\" Result: " + s);

		myDb.createTransaction("abc");
		myDb.put("a", "foo", "abc");
		s = (String)myDb.get("a").get(); // returns "bar"
		System.out.println("Expected: \"bar\" Result: " + s);

		myDb.rollBackTransaction("abc");
		myDb.put("a", "foo", "abc"); // throws exception
		s = (String)myDb.get("a").get(); // returns "bar"
		System.out.println("Expected: \"bar\" Result: " + s);

	}
}
