package com.db.model;

import org.junit.Test;

import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by lynnscott on 7/29/17.
 */
public class DataStoreTest {

    @Test
    public void putDb() throws Exception {
        DataStore ds = new DataStore();
        ds.put("example", "foo");
        assert("foo".equals(ds.get("example").get()));
    }

    @Test
    public void putDbTransaction() throws Exception {
        DataStore ds = new DataStore();
        ds.put("example", "foo", "xyz");
        assert("foo".equals(ds.get("example", "xyz").get()));
    }

    @Test
    public void getDBNotFound() throws Exception {
        DataStore ds = new DataStore();
        ds.get("example").ifPresent(name -> assertFalse(name == null));
    }

    @Test(expected = NoSuchElementException.class)
    public void getDbTransactionNotFound() throws Exception {
        DataStore ds = new DataStore();
        ds.get("example","xyz");
    }

    @Test
    public void deleteDbEntry() throws Exception {
        DataStore ds = new DataStore();
        ds.put("example", "foo");
        assert("foo".equals(ds.get("example").get()));
        ds.delete("example");
        ds.get("example").ifPresent(name -> assertFalse(name == null));
    }

    @Test
    public void deleteDbTransaction() throws Exception {
        DataStore ds = new DataStore();
        ds.put("example", "foo", "xyz");
        assert("foo".equals(ds.get("example", "xyz").get()));
        ds.delete("example", "xyz");
        ds.get("example","xyz").ifPresent(name -> assertFalse(name == null));
    }

    @Test(expected = NoSuchElementException.class)
    public void rollBackTransaction() throws Exception {
        DataStore ds = new DataStore();
        ds.put("example", "foo", "xyz");
        assert("foo".equals(ds.get("example", "xyz").get()));
        ds.rollBackTransaction("xyz");
        ds.get("example","xyz");

    }

    @Test
    public void commitTransaction() throws Exception {
        DataStore ds = new DataStore();
        ds.put("example", "foo");
        assert("foo".equals(ds.get("example").get()));

        ds.put("example", "bar", "xyz");
        assert("bar".equals(ds.get("example", "xyz").get()));

        try {
            ds.commitTransaction("xyz");
        } catch (NoSuchElementException | IllegalStateException ex) {
            //swollow exception
        }
        assert("bar".equals(ds.get("example", "xyz").get()));

    }

    @Test
    public void createAnExistingTransaction() throws Exception {
        DataStore ds = new DataStore();
        ds.createTransaction("xyz");
        ds.createTransaction("xyz");
    }



}